import { Component, OnInit, OnChanges, AfterViewInit, Input, Output, EventEmitter,
  ViewChild, ElementRef, NgZone, OnDestroy, HostListener} from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';

const buttonS = 100;
const corazonS = 50;

const nube1w = 100;
const nube1h = 60;

const nube2w = 150;
const nube2h = 70;

const arbol1w = 90;
const arbol1h = 100;

const arbol2w = 90;
const arbol2h = 150;

const arbol3w = 60;
const arbol3h = 100;

const puntajeMax = 100;

let cantidadVidas = 4;

const velocidades = [40, 50, 60, 70];
const posiciones = [275, 345, 415];

function randomNumber(min: number, max: number) {
  const numberR = Math.floor(Math.random() * (max - min + 1) + min);
  return numberR;
}
export enum EstadoTori {
  NORMAL,
  LOVELY,
  DAMAGE
}
export class Objetos {
  nombre: string;

  x = 0;
  y = 0;
  w: number;
  h: number;

  valor: number;

  private img: any;

  goodImages = [];
  badImages = [];
  esColisionable = false;
  constante = false;
  velocidad = velocidades[randomNumber(2, 3)];

  movimiento = 0;
  imgPregunta = [];

  constructor(private ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number,
              esColisionable: boolean, esConstante: boolean, img: any, nombre: string, valor?: number) {
                this.nombre = nombre;
                this.x = x;
                this.y = y;
                this.w = w;
                this.h = h;
                this.esColisionable = esColisionable;
                this.constante = esConstante;
                this.img = img;
                this.velocidad = nombre === 'finish' ? velocidades[0] : velocidades[3];
                this.valor = valor;
              }

  moveLeft() {
    this.x = this.x - this.velocidad;
    this.draw();
    if ( this.x + this.w <= 0) {
      this.x = this.ctx.canvas.width ;
      if ( ! this.constante ) {
        if ( !this.esColisionable ) {
          this.y = this.h * randomNumber(0, 1);
          // this.velocidad = velocidades[3];
        } else {
          this.x = this.x + 100;
          this.y = posiciones[randomNumber(0, 2)];
          this.velocidad = velocidades[randomNumber(0, 3)];
          if ( this.valor < 0) {
            this.img = this.badImages[randomNumber(0, this.badImages.length - 1 )];
          } else if ( this.valor > 0) {
            this.img = this.goodImages[randomNumber(0, this.goodImages.length - 1 )];
          }
        }
      }
      this.draw();
    }
  }
  moveCenter() {
    if ( this.x > ((this.ctx.canvas.width / 2) - (this.w / 2))) {
      this.x = this.x - velocidades[0] - 10;
    } else {
      this.x = ((this.ctx.canvas.width / 2) - (this.w / 2));
    }
    this.movimiento = this.movimiento === 0 ? 1 : 0;
    this.img = this.imgPregunta[this.movimiento];
    this.draw();

  }

  colision(x: any, y: any) {
    if (( x + 100 > this.x) && (this.x > x ) &&
    (y < (this.y + (this.h / 2))) &&
    ( (this.y + (this.h / 2)) < y + 50)) {
      this.x = this.ctx.canvas.width ;
      this.x = this.x + 100;
      this.y = posiciones[randomNumber(0, 2)];
      this.velocidad = velocidades[randomNumber(0, 3)];
      if ( this.valor < 0) {
        this.img = this.badImages[randomNumber(0, this.badImages.length - 1 )];
      } else if ( this.valor > 0) {
        this.img = this.goodImages[randomNumber(0, this.goodImages.length - 1 )];
      }
      this.draw();
      return this.valor;
    }
    return false;
  }

  draw() {
    this.ctx.save();
    this.ctx.drawImage(this.img, this.x, this.y, this.w, this.h);
    this.ctx.restore();
  }
}
export class Torti {
  private img: any;
  x = 0;
  y = 0;
  w: number;
  h: number;
  private estado: EstadoTori = EstadoTori.NORMAL;
  private movimiento = 0;
  private movimientosEstado = 0;

  private imagesNormal = [];
  private imagesAngry = [];
  private imagesLovely = [];

  constructor(private ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number,
              imgN: any, imgA: any, imgL: any) {
                this.x = x;
                this.y = y;
                this.w = w;
                this.h = h;
                this.imagesNormal = imgN;
                this.imagesAngry = imgA;
                this.imagesLovely = imgL;
                this.img = this.imagesNormal[0];
              }
/**
 * 275
 * 345
 * 415
 */
  moveUp() {
    if ( this.y === 275) {
      this.y = 275;
    } else if ( this.y === 345 ) {
      this.y = 275;
    } else if ( this.y === 415 ) {
      this.y = 345;
    }
  }
  moveDown() {
    if ( this.y === 275) {
      this.y = 345;
    } else if ( this.y === 345 ) {
      this.y = 415;
    } else if ( this.y === 415 ) {
      this.y = 415;
    }
  }
  moveToCenter() {
    this.ctx.drawImage(this.img, this.x, this.y, this.w, this.h);

    this.x += 10;
    if ( this.y < posiciones[2]) {
      this.y += 5;
    }
    if ( this.x >= (this.ctx.canvas.width / 2) - 25 ) {
      this.x = (this.ctx.canvas.width / 2) - 25 ;
      this.img = this.imagesLovely[this.movimiento];
    } else {
      this.movimiento = this.movimiento === 0 ? 1 : 0;
      this.img = this.imagesNormal[this.movimiento];
    }
  }
  setColision(tipo: EstadoTori) {
    this.estado = tipo;
    if ( this.estado === EstadoTori.LOVELY ) {
      this.movimientosEstado = 5;
    } else if ( this.estado === EstadoTori.DAMAGE ) {
      this.movimientosEstado = 5;
    }
  }
  colision(x: any, y: any) {
    if ((x > this.x) &&
    (x < this.x + this.w) &&
    (y > this.y) &&
    (y < this.y + this.h)) {
      return true;
    }
    return false;
  }
  callTorti() {
    this.ctx.drawImage(this.img, this.x, this.y, this.w, this.h);
  }
  draw() {
    if ( this.estado === EstadoTori.NORMAL) {
      this.img = this.imagesNormal[this.movimiento];
    } else if ( this.estado === EstadoTori.LOVELY ) {
      this.img = this.imagesLovely[this.movimiento];
    } else if ( this.estado === EstadoTori.DAMAGE ) {
      this.img = this.imagesAngry[this.movimiento];
    }
    this.ctx.drawImage(this.img, this.x, this.y, this.w, this.h);
    this.movimientosEstado = this.movimientosEstado - 1;

    if ( this.movimientosEstado === 0 ) {
      this.estado = EstadoTori.NORMAL;
    }
    this.movimiento = this.movimiento === 0 ? 1 : 0;
  }
}
export class Boton {

  nombre: string;

  private img: any;
  x = 0;
  y = 0;

  private z = buttonS;

  constructor(private ctx: CanvasRenderingContext2D) {}

  variables(img: any, x: any, y: any, nombre: string) {
    this.img = img;
    this.x = x;
    this.y = y;
    this.nombre = nombre;
  }

  colision(x: any, y: any) {
    if ((x > this.x) &&
    (x < this.x + this.z) &&
    (y > this.y) &&
    (y < this.y + this.z)) {
      return true;
    }
    return false;
  }

  draw() {
    this.ctx.drawImage(this.img, this.x, this.y, this.z, this.z);
  }
}
export class Corazon {
  valido = true;

  private img: any;
  private x = 0;
  private y = 0;

  private z = corazonS;

  constructor(private ctx: CanvasRenderingContext2D, img: any, x: number, y: number) {
    this.img = img;
    this.x = x;
    this.y = y;
  }
  draw() {
    this.ctx.drawImage(this.img, this.x, this.y, this.z, this.z);
  }
}

@Component({
  selector: 'app-juego',
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.css']
})


export class JuegoComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy   {

  onclicStar = true;
  showLogo = false;
  showCanvas = false;
  isFadeOut = false;

  startMsg = true;
  goMsg = false;
  onPlay = false;
  onFinish = false;
  readyToAnswer = false;

  requestId;
  interval;

  // botones: Boton[] = [];
  botonSi: Boton;
  botonNo: Boton;

  corazones: Corazon[] = [];
  objetosPaisaje: Objetos[] = [];
  objetosComida: Objetos[] = [];
  meta: Objetos;
  pregunta: Objetos;
  torti: Torti;

  respuestaSi = false;

  imagenes = ['food_1', 'food_2', 'food_3', 'food_4', 'food_5', 'finish', 'go', 'ready', 'question_1', 'question_2', 'torti_1', 'torti_2',
              'torti_love_1', 'torti_love_2', 'torti_sad_1', 'torti_sad_2', 'answer_si', 'answer_no', 'tree_1', 'tree_2', 'tree_3',
              'cloud_1', 'cloud_2', 'life', 'up', 'down'];
  imgObjetos: any = {};

  imgFondo = null;

  currentText = '123456';
  puntaje = 0;
  tiempoEnAparecer = 30;
  tiempoEnAparecerBotones = 20;

  @ViewChild('canvasGame', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if ( this.ctx ) {
      this.ctx.canvas.width = event.target.innerWidth - 80;
      console.log(this.ctx.canvas.width);
      // this.reset();
      // this.ctx.canvas.height = event.target.innerHeight - 10;
    }
  }

  // @HostListener('document:keypress', ['$event'])
  // handleKeyboardEvent(event: KeyboardEvent) {
  //   console.log(event.key);
  // }
  @HostListener('document:keydown', ['$event'])
  handleKeyDownEvent(event: KeyboardEvent) {
    this.procesaKey(event.key);
  }

  constructor(private ngZone: NgZone) { }

  ngOnInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    if (this.ctx) {

      this.ctx.canvas.width = window.innerWidth - 80;
      this.ngZone.runOutsideAngular(() => this.anima());

      // Cargar Fotos
      this.imagenes.forEach(element => {
        this.imgObjetos[element] = new Image();
        // this.imgColors[element].onload = () => {
        // };
        this.imgObjetos[element].src = 'assets/img/' + element + '.png';
      });

      this.imgFondo = new Image();
      this.imgFondo.src = 'assets/img/tortiFondo.jpg';

      cantidadVidas = 4;

      setInterval(() => {
          this.anima();
      }, 210);
    } else {
      alert('No cuentas con Canvas');
    }
  }
  ngOnChanges() {
  }
  ngAfterViewInit() {
    document.addEventListener('mousemove', (e) => {
      if (!this.readyToAnswer) { return; }
      const pos = this.ajusta(e.clientX, e.clientY);
      const x = pos.x;
      const y = pos.y;

      const colisionBoton = this.botonNo.colision(x, y);

      if (colisionBoton ) {
        this.botonNo.x  = randomNumber( 50, this.ctx.canvas.width - 50);
        this.botonNo.y = randomNumber( 50, this.ctx.canvas.height - 50);
      }
    });
  }

  onClicImage() {
    this.onclicStar = false;
    this.showLogo = true;
    this.showCanvas = false;
    setTimeout(() => {
      this.isFadeOut = true;
      this.onEndLogo();
    }, 2000);
  }
  onEndLogo() {
    this.onclicStar = false;

    setTimeout(() => {
      this.showLogo = false;
      this.showCanvas = true;

      this.reset();
    }, 2000);
  }

  mensaje(valor: string) {
    this.ctx.save();
    this.ctx.strokeStyle = 'orange';
    this.ctx.fillStyle = 'orange';
    this.ctx.clearRect(0, 460, this.ctx.canvas.width, 100);
    this.ctx.font = 'bold 50px Courier New';
    this.ctx.textAlign = 'center';
    this.ctx.fillText(valor, this.ctx.canvas.width / 2, this.ctx.canvas.height / 2);
    this.ctx.restore();
  }

  msjPuntaje() {
    this.ctx.save();

    this.ctx.fillStyle = '#663A31';
    this.ctx.fillRect(0, this.ctx.canvas.height - 60, this.ctx.canvas.width, 60);

    this.ctx.strokeStyle = 'white';
    this.ctx.fillStyle = 'white';
    this.ctx.font = 'bold 30px sans-serif';
    this.ctx.fillText('Puntaje:  ' + String(this.puntaje), (this.ctx.canvas.width / 2) - 80, this.ctx.canvas.height - 20);
    this.ctx.restore();
  }

  anima() {
    this.resetCanvas();

    if ( this.showCanvas ) {
      if ( this.startMsg ) {
        this.ctx.drawImage(this.imgObjetos.ready, this.ctx.canvas.width / 2 - 150, this.ctx.canvas.height / 2 - 200,
                          300, 100);
        setTimeout(() => {
          this.startMsg = false;
          this.goMsg = true;
          this.onPlay = false;
        }, 1000);
      }
      if ( this.goMsg ) {
        this.ctx.drawImage(this.imgObjetos.go, this.ctx.canvas.width / 2 - 75, this.ctx.canvas.height / 2 - 200,
          150, 100);
        setTimeout(() => {
          this.startMsg = false;
          this.goMsg = false;
          this.onPlay = true;
        }, 1000);
      }
      if ( this.onFinish ) {

        this.torti.moveToCenter();
        // this.torti.draw();

        if (this.tiempoEnAparecer <= 0 ) {
          this.pregunta.moveCenter();
          if ( this.tiempoEnAparecerBotones <= 0) {
            this.botonSi.draw();
            this.botonNo.draw();
            this.readyToAnswer = true;
          } else {
            this.tiempoEnAparecerBotones --;
          }
        } else {
          this.tiempoEnAparecer --;
        }

      } else if ( this.onPlay ) {

        this.objetosPaisaje.forEach(element => {
          element.moveLeft();
        });
        if ( this.puntaje >= puntajeMax ) {
          this.meta.moveLeft();
        }
        this.objetosComida.forEach(element => {
          const colision = element.colision(this.torti.x, this.torti.y);

          // console.log(colision);

          if ( colision ) {
            if (colision > 0 ) {
              this.torti.setColision(EstadoTori.LOVELY);
              this.puntaje = this.puntaje + colision;
              // if ( this.puntaje >= puntajeMax) {
              //   this.onFinish = true;
              //   this.onPlay = false;
              // }
            } else {
              this.torti.setColision(EstadoTori.DAMAGE);
              this.corazones.pop();
              if ( this.corazones.length === 0) {
                this.onPlay = false;
                this.reset();
                this.resetCanvas();
              }
            }
          }
          if ( this.onPlay ) {
            element.moveLeft();
          }
        });
        const valueFinish = this.torti.x + this.torti.w >= this.meta.x ? true : false;
        if ( valueFinish ) {
          this.onFinish = true;
          this.onPlay = false;
        }
        if ( this.onPlay ) {
          this.torti.draw();
        }
      }

    }

    this.requestId = requestAnimationFrame(() => this.anima);
  }

  procesaKey(keyCode: string) {
    if ( this.onPlay ) {
      if ( keyCode === 'ArrowUp' ) {
        this.torti.moveUp();
      } else if ( keyCode === 'ArrowDown') {
        this.torti.moveDown();
      } else if ( keyCode === 's') {
        this.torti.moveDown();
      } else if ( keyCode === 'w') {
        this.torti.moveUp();
      }
    }
  }

  ngOnDestroy() {
    clearInterval(this.interval);
    cancelAnimationFrame(this.requestId);
  }

  resetCanvas() {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

    if ( this.showCanvas ) {
      // Fondo
      if ( !this.onFinish ) {
        this.ctx.drawImage(this.imgFondo, 0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.msjPuntaje();
        // this.botones.forEach(element => {
        //   element.draw();
        // });
        // this.corazones.forEach(element => {
        //   element.draw();
        // });
      }

      this.torti.callTorti();
    }
    this.ctx.restore();
  }
  ajusta(xx, yy) {
    const posCanvas = this.ctx.canvas.getBoundingClientRect();

    const x = xx - posCanvas.left;
    const y = yy - posCanvas.top;

    return {
        x,
        y
    };
  }
  selecciona(e: any) {
    const pos = this.ajusta(e.clientX, e.clientY);
    const x = pos.x;
    const y = pos.y;

    this.procesaSeleccion(x, y);
  }
  handleStart(e: any) {
    const pos = this.ajusta(e.touches[0].clientX, e.touches[0].clientY);
    const x = pos.x;
    const y = pos.y;

    this.procesaSeleccion(x, y);
  }
  handleEnd(e: any) {
    const pos = this.ajusta(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
    const x = pos.x;
    const y = pos.y;

    this.procesaSeleccion(x, y);
  }
  btnPressUp() {
    this.torti.moveUp();
  }
  btnPressDown() {
    this.torti.moveDown();
  }
  procesaSeleccion(x, y) {
    if ( this.onPlay ) {
      // Colisiones
      // this.botones.forEach(element => {
      //   if ( element.colision(x, y) ) {
      //     if ( element.nombre === 'up') {
      //       this.torti.moveUp();
      //     }
      //     if ( element.nombre === 'down') {
      //       this.torti.moveDown();
      //     }
      //   }
      // });
    }
    if ( this.readyToAnswer ) {
      if ( this.botonSi.colision(x, y)) {
        this.respuestaSi = true;
        //
      }
      if ( this.botonNo.colision(x, y)) {
        this.botonNo.x  = randomNumber( 50, this.ctx.canvas.width - 50);
        this.botonNo.y = randomNumber( 50, this.ctx.canvas.height - 50);
      }
    }
  }

  reset() {
    // this.botones = [];
    this.botonSi = null;
    this.botonNo = null;

    this.corazones = [];
    this.objetosComida = [];
    this.objetosPaisaje = [];
    this.torti = null;
    this.meta = null;
    this.pregunta = null;

    this.puntaje = 0;
    this.startMsg = false;
    this.startMsg = true;
    this.goMsg = false;
    this.onFinish = false;
    this.respuestaSi = false;

    this.tiempoEnAparecer = 30;
    this.tiempoEnAparecerBotones = 20;
    this.readyToAnswer = false;

    this.botonSi = new Boton(this.ctx);
    this.botonSi.variables(this.imgObjetos.answer_si, this.ctx.canvas.width / 2 - buttonS, posiciones[0], 'answer_si');
    this.botonNo = new Boton(this.ctx);
    this.botonNo.variables(this.imgObjetos.answer_no, this.ctx.canvas.width / 2 + 20, posiciones[0], 'answer_no');

    // const boton1 = new Boton(this.ctx);
    // boton1.variables(this.imgObjetos.up, this.ctx.canvas.width / 2 - buttonS, this.ctx.canvas.height - buttonS, 'up');
    // this.botones.push(boton1);

    // const boton2 = new Boton(this.ctx);
    // boton2.variables(this.imgObjetos.down, this.ctx.canvas.width / 2 + 20, this.ctx.canvas.height - buttonS, 'down');
    // this.botones.push(boton2);

    this.torti = new Torti(this.ctx, 70, 345 , 100, 50, [this.imgObjetos.torti_1, this.imgObjetos.torti_2],
      [this.imgObjetos.torti_sad_1, this.imgObjetos.torti_sad_2], [this.imgObjetos.torti_love_1, this.imgObjetos.torti_love_2]);

    this.meta = new Objetos(this.ctx, this.ctx.canvas.width, posiciones[0], 80, 180, true, true, this.imgObjetos.finish, 'finish', 100);
    this.pregunta = new Objetos(this.ctx, this.ctx.canvas.width, 20, 400, 200, true, true, this.imgObjetos.question_1, 'question_1');
    this.pregunta.imgPregunta = [this.imgObjetos.question_1, this.imgObjetos.question_2];

    for (let i = 0; i < cantidadVidas; i++) {
      this.corazones.push(new Corazon(this.ctx, this.imgObjetos.life, 50 + ( corazonS * i + 10 * i),
                          this.ctx.canvas.height - ( 1.5 * corazonS)));
    }
    this.objetosPaisaje.push(new Objetos(this.ctx, this.ctx.canvas.width,
      10, nube1w, nube1h, false, false, this.imgObjetos.cloud_1, 'cloud_1' ));

    this.objetosPaisaje.push(new Objetos(this.ctx, this.ctx.canvas.width + (this.ctx.canvas.width / 2),
        30, nube2w, nube2h, false, false, this.imgObjetos.cloud_2, 'cloud_2' ));

    this.objetosPaisaje.push(new Objetos(this.ctx, this.ctx.canvas.width + (this.ctx.canvas.width / 4),
      274 - arbol1h, arbol1w, arbol1h, false, true, this.imgObjetos.tree_1, 'tree_1' ));

    this.objetosPaisaje.push(new Objetos(this.ctx, this.ctx.canvas.width + 2 * (this.ctx.canvas.width / 3) + (this.ctx.canvas.width / 4),
        274 - arbol2h, arbol2w, arbol2h, false, true, this.imgObjetos.tree_2, 'tree_2' ));

    this.objetosPaisaje.push(new Objetos(this.ctx, this.ctx.canvas.width + 3 * (this.ctx.canvas.width / 3) + (this.ctx.canvas.width / 4),
        274 - arbol3h, arbol3w, arbol3h, false, true, this.imgObjetos.tree_3, 'tree_3' ));

    const newBad = new Objetos(this.ctx, this.ctx.canvas.width,
      posiciones[0], 50, 50, true, false, this.imgObjetos.food_1, 'food_1', -5 );
    newBad.badImages = [this.imgObjetos.food_1, this.imgObjetos.food_2];
    this.objetosComida.push(newBad);
    const newBad2 = new Objetos(this.ctx, this.ctx.canvas.width + (this.ctx.canvas.width / 2),
    posiciones[1], 50, 50, true, false, this.imgObjetos.food_2, 'food_2', -5 );
    newBad2.badImages = [this.imgObjetos.food_1, this.imgObjetos.food_2];
    this.objetosComida.push(newBad2);

    const newGood = new Objetos(this.ctx, this.ctx.canvas.width + (this.ctx.canvas.width / 4),
      posiciones[3], 50, 50, true, false, this.imgObjetos.food_1, 'food_3', 10 );
    newGood.goodImages = [this.imgObjetos.food_3, this.imgObjetos.food_4, this.imgObjetos.food_5];
    this.objetosComida.push(newGood);
    const newGoo2 = new Objetos(this.ctx, this.ctx.canvas.width + (this.ctx.canvas.width / 2),
    posiciones[1], 50, 50, true, false, this.imgObjetos.food_2, 'food_5', 10 );
    newGoo2.goodImages = [this.imgObjetos.food_3, this.imgObjetos.food_4, this.imgObjetos.food_5];
    this.objetosComida.push(newGoo2);
    // posiciones
    // this.ctx.canvas.addEventListener('keydown', (e) => {
    //   console.log(e);
    // });
    // this.ctx.canvas.addEventListener('keyup', (e) => {

    // });
    this.ctx.canvas.addEventListener('click', this.selecciona.bind(this));
    // this.ctx.canvas.addEventListener('touchstart', this.handleStart.bind(this));
    // this.ctx.canvas.addEventListener('touchend', this.handleEnd.bind(this));

  }
}
